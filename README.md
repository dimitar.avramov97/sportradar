# Sportradar Project - Live Football World Cup Score Board

A Java library for managing live football match scores. This library allows you to create and update football match scores, finish matches, and retrieve a summary of ongoing matches.

## Features

- Start new football matches with initial scores (0 - 0).
- Update scores for ongoing matches.
- Finish matches in progress.
- Get a summary of ongoing matches ordered by total score first, and if same, ordered my the recent match.

## Prerequisites

- Java Development Kit (JDK) installed on your machine.

## Test-Driven Development (TDD) Approach

This project follows the Test-Driven Development (TDD) methodology, where each step of the TDD cycle is accompanied by a corresponding merge request.

