package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Scoreboard {
    private List<FootballMatch> matches;

    public Scoreboard() {
        matches = new ArrayList<>();
    }

    public void addMatch(String homeTeam, String awayTeam) {
        FootballMatch match = new FootballMatch(homeTeam, awayTeam);
        matches.add(match);
    }

    public List<FootballMatch> getMatchesInProgress() {
        return matches.stream()
                .sorted(Collections.reverseOrder(Comparator.comparingInt(FootballMatch::getTotalScore))
                        .thenComparing(Collections.reverseOrder(Comparator.comparingInt(matches::indexOf))))
                .toList();
    }

    public void finishMatch(int matchIndex) {
        if (matches.isEmpty()) {
            throw new IllegalStateException("No matches in progress to finish.");
        }

        if (matchIndex >= 0 && matchIndex < matches.size()) {
            matches.remove(matchIndex);
        } else {
            throw new IllegalArgumentException("Invalid match index");
        }
    }

    public void updateScore(int matchIndex, int homeScore, int awayScore) {
        if (matches.isEmpty()) {
            throw new IllegalStateException("No matches in progress to finish.");
        }

        if (matchIndex >= 0 && matchIndex < matches.size()) {
            FootballMatch match = matches.get(matchIndex);
            match.updateScore(homeScore, awayScore);
        } else {
            throw new IllegalArgumentException("Invalid match index");
        }
    }
}
