package test;

import main.FootballMatch;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FootballMatchTest {

    @Test
    public void testUpdateScore() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        match.updateScore(0, 5);
        assertEquals(0, match.getHomeScore());
        assertEquals(5, match.getAwayScore());
    }

    @Test
    public void testGetTotalScore() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        match.updateScore(0, 5);
        assertEquals(5, match.getTotalScore());
    }

    @Test
    public void testGetHomeTeam() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        assertEquals("Mexico", match.getHomeTeam());
    }

    @Test
    public void testGetAwayTeam() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        assertEquals("Canada", match.getAwayTeam());
    }

    @Test
    public void testUpdateScoreWithNegativeValues() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        match.updateScore(-1, 3);
        assertEquals(0, match.getHomeScore());
        assertEquals(3, match.getAwayScore());
    }

    @Test
    public void testInitialScoresAreZero() {
        FootballMatch match = new FootballMatch("Mexico", "Canada");
        assertEquals(0, match.getHomeScore());
        assertEquals(0, match.getAwayScore());
    }
}
