package test;

import main.FootballMatch;
import main.Scoreboard;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreboardTest {

    @Test
    public void testAddMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");
        assertEquals(1, scoreboard.getMatchesInProgress().size());
    }

    @Test
    public void testFinishMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");
        scoreboard.addMatch("Mexico", "Canada");
        assertEquals(2, scoreboard.getMatchesInProgress().size());

        scoreboard.finishMatch(0);
        assertEquals(1, scoreboard.getMatchesInProgress().size());
    }

    @Test
    public void testFinishInvalidMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");

        assertThrows(IllegalArgumentException.class, () -> scoreboard.finishMatch(1));

        assertEquals(1, scoreboard.getMatchesInProgress().size());
    }

    @Test
    public void testFinishMatchWithEmptyMatches() {
        Scoreboard scoreboard = new Scoreboard();

        assertThrows(IllegalStateException.class, () -> {
            scoreboard.finishMatch(0);
        });
    }

    @Test
    public void testUpdateScore() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");
        scoreboard.updateScore(0, 2, 1);
        assertEquals(2, scoreboard.getMatchesInProgress().get(0).getHomeScore());
        assertEquals(1, scoreboard.getMatchesInProgress().get(0).getAwayScore());
    }

    @Test
    public void testUpdateScoreWithInvalidMatchIndex() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");

        assertThrows(IllegalArgumentException.class, () -> scoreboard.updateScore(1, 2, 1));

        assertEquals(0, scoreboard.getMatchesInProgress().get(0).getHomeScore());
        assertEquals(0, scoreboard.getMatchesInProgress().get(0).getAwayScore());
    }

    @Test
    public void testUpdateScoreWithEmptyMatches() {
        Scoreboard scoreboard = new Scoreboard();

        assertThrows(IllegalStateException.class, () -> scoreboard.updateScore(1,1,1));
    }

    @Test
    public void testGetMatchesInProgress() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");
        scoreboard.addMatch("Spain", "Brazil");
        scoreboard.updateScore(0, 1, 2);
        scoreboard.updateScore(1, 4, 0);

        List<FootballMatch> matchesInProgress = scoreboard.getMatchesInProgress();

        assertEquals("Spain", matchesInProgress.get(0).getHomeTeam());
        assertEquals("Brazil", matchesInProgress.get(0).getAwayTeam());
        assertEquals("Mexico", matchesInProgress.get(1).getHomeTeam());
        assertEquals("Canada", matchesInProgress.get(1).getAwayTeam());
    }

    @Test
    public void testGetMatchesInProgressSameTotalScore() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.addMatch("Mexico", "Canada");
        scoreboard.addMatch("Spain", "Brazil");
        scoreboard.addMatch("Germany", "France");
        scoreboard.addMatch("Uruguay", "Italy");
        scoreboard.addMatch("Argentina", "Australia");
        scoreboard.updateScore(0, 0, 5);
        scoreboard.updateScore(1, 10, 2);
        scoreboard.updateScore(2, 2, 2);
        scoreboard.updateScore(3, 6, 6);
        scoreboard.updateScore(4, 3, 1);

        List<FootballMatch> matchesInProgress = scoreboard.getMatchesInProgress();

        assertEquals("Uruguay", matchesInProgress.get(0).getHomeTeam());
        assertEquals("Italy", matchesInProgress.get(0).getAwayTeam());
        assertEquals("Spain", matchesInProgress.get(1).getHomeTeam());
        assertEquals("Brazil", matchesInProgress.get(1).getAwayTeam());
        assertEquals("Mexico", matchesInProgress.get(2).getHomeTeam());
        assertEquals("Canada", matchesInProgress.get(2).getAwayTeam());
        assertEquals("Argentina", matchesInProgress.get(3).getHomeTeam());
        assertEquals("Australia", matchesInProgress.get(3).getAwayTeam());
        assertEquals("Germany", matchesInProgress.get(4).getHomeTeam());
        assertEquals("France", matchesInProgress.get(4).getAwayTeam());
    }
}
